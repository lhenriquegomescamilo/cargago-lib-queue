package com.cargago.queue.config

import org.slf4j.LoggerFactory
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageListener
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.stereotype.Service


@Service
class QueueListenerConfig(private val messageConverter: MessageConverter) : MessageListener {
  private val log = LoggerFactory.getLogger(javaClass)

  override fun onMessage(message: Message?) {
    log.info("receive message from ${message?.messageProperties?.consumerQueue}")
    message?.let {
      val messageConverted = messageConverter.fromMessage(message)
      log.info("Message $messageConverted")
    }


  }
}