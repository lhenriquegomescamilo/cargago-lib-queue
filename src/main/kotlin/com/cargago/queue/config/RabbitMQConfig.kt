package com.cargago.queue.config

import org.springframework.amqp.core.*
import org.springframework.amqp.core.Binding.DestinationType.QUEUE
import org.springframework.stereotype.Component

enum class TypeExchange(name: String) {
  DIRECT("amq.direct"),
  FANOUT("amq.fanout"),
  TOPIC("amq.topic");

}
@Component
class RabbitMQConfig(private val amqpAdmin: AmqpAdmin) {

  private fun queue(name: String) = Queue(name, true)

  private fun exchange(typeExchange: TypeExchange): Exchange {
    return ExchangeBuilder
      .directExchange(typeExchange.name)
      .durable(true)
      .build()
  }

  private fun binding(queue: Queue, exchange: Exchange) =
    Binding(queue.name, QUEUE, exchange.name, queue.name, emptyMap())

  fun createQueue(name: String,typeExchange: TypeExchange) {
    val queue = queue(name)

    val exchange = exchange(typeExchange)
    val binding = binding(queue, exchange)

    this.amqpAdmin.declareQueue(queue)
    this.amqpAdmin.declareExchange(exchange)
    this.amqpAdmin.declareBinding(binding)

  }
}

