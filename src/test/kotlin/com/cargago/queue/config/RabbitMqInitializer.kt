package com.cargago.queue.config

import org.junit.ClassRule
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.utility.DockerImageName



abstract class RabbitMqInitializer {

    companion object {
        @ClassRule
        private val rabbit = GenericContainer(DockerImageName.parse("rabbitmq:3.9.20-management-alpine")).apply {
            withExposedPorts(5672, 15672)
            withEnv(
                mapOf(
                    "RABBITMQ_DEFAULT_USER" to "cargago",
                    "RABBITMQ_DEFAULT_PASS" to "cargago"
                )
            )
            start()
        }


        class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {

            override fun initialize(applicationContext: ConfigurableApplicationContext) {
                rabbit.waitingFor(Wait.forHttp("/"))
                TestPropertyValues.of(
                    "spring.rabbitmq.host=${rabbit.host}",
                    "spring.rabbitmq.port=${rabbit.getMappedPort(5672)}",
                    "spring.rabbitmq.username=cargago",
                    "spring.rabbitmq.password=cargago"
                ).applyTo(applicationContext)
            }
        }
    }

}